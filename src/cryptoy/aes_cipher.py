from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    aesgcm = AESGCM(key)
    cipher = aesgcm.encrypt(nonce, msg, None)
    return cipher


def decrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    # A implémenter en utilisant la class AESGCM
    aesgcm = AESGCM(key)
    decipher = aesgcm.decrypt(nonce, msg, None)
    return decipher
